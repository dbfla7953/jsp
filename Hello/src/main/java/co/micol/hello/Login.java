package co.micol.hello;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login") // 이게 호출하는 것. 슬래쉬 빼고 넣어줘야 함
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get 호출시 처리하는 곳
		request.setCharacterEncoding("utf-8"); // 전달받은 한글 깨짐 방지
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("utf-8"); // 돌려줄 값 한글 깨짐 방지. login.jsp에서 스트링값 복붙.
		
		String name = request.getParameter("name"); // 로그인서블릿 name을 name에 담겠다.
		String password = request.getParameter("password");
		
//		System.out.println(name + " : " + password ); // 제대로 값 넘어갔는지 확인하기 위해 사용했음
		if(name.equals("홍길동") && password.equals("1234")) {
			response.getWriter().append("로그인 성공");
		}else {
			response.getWriter().append("로그인 실패");
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// post 호출시 처리하는 곳
		doGet(request, response); // get방식, post방식 하나만 해놓으면 됨.
	}

}

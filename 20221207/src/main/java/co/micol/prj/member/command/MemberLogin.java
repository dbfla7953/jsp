package co.micol.prj.member.command;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.micol.prj.common.AES256Util;
import co.micol.prj.common.Command;
import co.micol.prj.member.service.MemberService;
import co.micol.prj.member.service.MemberVO;
import co.micol.prj.member.serviceImpl.MemberServiceImpl;

public class MemberLogin implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 로그인 처리
		MemberService dao = new MemberServiceImpl(); // DB에 가야하니까 일단 호출
		MemberVO vo = new MemberVO(); // 정보를 vo에 담아야하니까 호출
		
		HttpSession session = request.getSession(); // 서버가 만들어 보관하고 있는 세션객체를 호출
	
		String password = request.getParameter("memberPassword");
		
		try {
			AES256Util aes = new AES256Util();
			try {
				password = aes.encrypt(password);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // 암호호ㅏ됨
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
//		System.out.println(password + "===================="); // 제대로 암호화 되는지확인용
		
		String message = null;
		
		vo.setMemberId(request.getParameter("memberId"));
		vo.setMemberPassword(password); // 암호화된 상태인 패스워드를 넘겨줘야함
		
		vo = dao.memberSelect(vo);
		if(vo != null) {
			session.setAttribute("id", vo.getMemberId());
			session.setAttribute("author", vo.getMemberAuthor());
			session.setAttribute("name", vo.getMemberName()); // 세션객체에 넣어준다!
			
			message = vo.getMemberName() + "님 환영합니다.";
			request.setAttribute("message", message);
//			request.setAttribute("member", vo); ??????? 왜 막는겨
		}else {
			message = "아이디 또는 패스워드가 틀렸습니다!";
			request.setAttribute("message", message);
		}
		return "member/memberLogin.tiles"; // jsp만들어주러가야함
	}

}

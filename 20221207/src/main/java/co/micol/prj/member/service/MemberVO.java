package co.micol.prj.member.service;

import lombok.Getter;
import lombok.Setter;

//DTO => DAO에 값을 실어서 줌
//@Data //-> 어노테이션 쓰면 자동으로 롬복이 게터세터
// -> 투스트링 만듦(java에서는 투스트링도 필요하니까(콘솔창에서 띄워야하니까) 걍 @data쓰는게 편함)
@Getter
@Setter // jsp에서는 게터세터만 필요하니까 따로 만들어줌
public class MemberVO { // DTO(DAO의 값을 실어서 옮김) . 테이블이름보고만들기
	private String memberId; // 언더바쓰지 않으니까 카멜표기법으로.
	private String memberName;
	private String memberPassword;
	private int memberAge;
	private String memberAddress;
	private String memberTel;
	private String memberAuthor;
}

package co.micol.prj.member.command;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.AES256Util;
import co.micol.prj.common.Command;
import co.micol.prj.member.service.MemberService;
import co.micol.prj.member.service.MemberVO;
import co.micol.prj.member.serviceImpl.MemberServiceImpl;

public class memberJoin implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 회원가입 처리
		MemberService dao = new MemberServiceImpl();
		MemberVO vo = new MemberVO();
		
		String password = request.getParameter("memberPassword");
		
		try {
			AES256Util aes = new AES256Util();
			try {
				password = aes.encrypt(password);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // 암호호ㅏ됨
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
//		System.out.println(password + "===================="); // 제대로 암호화 되는지확인용
		
		int n = 0;
//		String viewPage = null; // 돌려줄 페이지(안쓰니까막아놓음)
		String message = null; // 메세지담는..
		
		//매퍼 확인하고 어떤 값 넘겨주기로 했는지 확인 후 그대로 넘겨준다. 안 넘겨주면 null값으로 넘어감
		vo.setMemberId(request.getParameter("memberId"));
		vo.setMemberName(request.getParameter("memberName"));
		vo.setMemberPassword(password); // 암호화된 상태인 패스워드를 넘겨줘야함
		//나이를 필수로 받기 싫다면,
		if(request.getParameter("memberAge") != "") {
			vo.setMemberAge(Integer.valueOf(request.getParameter("memberAge")));
		}
//		vo.setMemberAge(Integer.valueOf(request.getParameter("memberAge"))); // form에서 넘어오는 값은 모두 스트링, 그러나 age는 int타입이니까 바꿔서 넣어줘야함
		vo.setMemberAddress(request.getParameter("memberAddress"));
		vo.setMemberTel(request.getParameter("memberTel"));
		vo.setMemberAuthor("USER");
		
		n = dao.memberInsert(vo);
		
		if(n != 0) { //0이 아니면, 즉 성공했다는 의미
//			message = "회원가입 성공!";
			return "memberList.do"; // 회원가입 성공하면 회원리스트 뜨게. 
		}else {
			message = "회원가입 실패!";
		}
		request.setAttribute("message", message); // 결과 담기
		
		return "member/memberJoin.tiles"; // 멤버조인 jsp 만들러 가야함
	}

}

package co.micol.prj.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.micol.prj.common.Command;

public class memberLogout implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 로그아웃 처리
		HttpSession session = request.getSession();
		String message = (String) session.getAttribute("name"); // 세션에는 오브젝트로 들어가있으니까 String으로 강제타입변환해서 가져옴.
		message += "님 정상적으로 로그아웃되었습니다!";
		session.invalidate(); // 세션 통째로 없애버림(로그아웃)
		request.setAttribute("message", message);
		
		return "member/memberLogin.tiles";
	}

}

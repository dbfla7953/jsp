package co.micol.prj.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.Command;
import co.micol.prj.member.service.MemberService;
import co.micol.prj.member.service.MemberVO;
import co.micol.prj.member.serviceImpl.MemberServiceImpl;
import oracle.jdbc.driver.Message;

public class memberSelect implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 멤버 한 명 조회
		MemberService dao = new MemberServiceImpl();
		MemberVO vo = new MemberVO();
		
		vo.setMemberId(request.getParameter("memberId"));
		vo = dao.memberSelect(vo);
		
		String viewPage = null;
		
		if(vo != null) { // 데이터가 존재하면 vo에 멤버 싣고 멤버셀렉트jsp로.
			request.setAttribute("member", vo);
			viewPage = "member/memberSelect.tiles"; // jsp만들어줘야함
		}else {
			request.setAttribute("message", "존재하지 않는 회원입니다.");
			viewPage = "member/memberLogin.tiles";
		}
		
		return viewPage;
	}

}


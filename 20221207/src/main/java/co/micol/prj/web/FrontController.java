package co.micol.prj.web;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.MainCommand;
import co.micol.prj.common.Command;
import co.micol.prj.member.command.AjaxMemberIdCheck;
import co.micol.prj.member.command.MemberList;
import co.micol.prj.member.command.MemberLogin;
import co.micol.prj.member.command.memberDelete;
import co.micol.prj.member.command.memberEdit;
import co.micol.prj.member.command.memberJoin;
import co.micol.prj.member.command.memberJoinForm;
import co.micol.prj.member.command.memberLoginForm;
import co.micol.prj.member.command.memberLogout;
import co.micol.prj.member.command.memberSelect;
import co.micol.prj.member.command.memberUpdate;

//@WebServlet("*.do")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private HashMap<String, Command> map = new HashMap<String, Command>();

	public FrontController() {
		super();

	}

	public void init(ServletConfig config) throws ServletException {
		// 명령집단 map.put(k,V) 형태로 담아두는 곳
		map.put("/main.do", new MainCommand()); // 처음 실행하는 페이지
		map.put("/memberList.do", new MemberList()); // 멤버목록보기
		map.put("/memberJoinForm.do", new memberJoinForm()); // 회원가입폼
		map.put("/AjaxMemberIdCheck.do", new AjaxMemberIdCheck()); // 회원아이디 중복체크
		map.put("/memberJoin.do",new memberJoin()); // 회원가입 처리
		map.put("/memberLoginForm.do",new memberLoginForm()); // 로그인 폼 호출
		map.put("/memberLogin.do", new MemberLogin()); // 로그인 처리
		map.put("/memberLogout.do", new memberLogout()); // 로그아웃 처리
		map.put("/memberSelect.do", new memberSelect()); // 멤버 한 명 조회
		map.put("/memberEdit.do", new memberEdit()); // 멤버 수정폼 호출
		map.put("/memberDelete.do", new memberDelete()); // 멤버 삭제
		map.put("/memberUpdate.do", new memberUpdate()); // 멤버 수정
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 요청을 분석, 실행, 결과를 돌려주는 곳
		request.setCharacterEncoding("utf-8"); // 한글깨짐방지
		String uri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String page = uri.substring(contextPath.length()); // 실제 요청 페이지가 나옴
//		System.out.println(page + "================="); // 이걸로 널포인트익셉션은 쉽게 잡을 수 있다!

		// 커맨드실행
		Command command = map.get(page); // 적절한 페이지를 찾아서 map이 키값 던져주면 밸류값 리턴시켜줌
		String viewPage = command.exec(request, response);

		// 그 결과를 어느 페이지에 뿌려줄지 결정
		// view Resolve start (어느페이지를 보여줄지를 결정하는 것 : view Resolve)
		if (!viewPage.endsWith(".do")) { // .do가 포함되어있지 않다면,
			if (viewPage.startsWith("Ajax:")) {
				// ajax
				response.setContentType("text/html; charset=UTF-8");
				response.getWriter().print(viewPage.substring(5)); // 왜 5야,,? 아 Ajax: 가 네글자라서
				return;
			} else if(!viewPage.endsWith(".tiles")){ // 타일즈 적용안하는것
				viewPage = "WEB-INF/views/" + viewPage + ".jsp"; // ex.만약 리턴값이 return "member/memberJoinForm" 이라면, jsp파일로 넘어가서 처리됨
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher(viewPage);
			dispatcher.forward(request, response);
		} else { // .do면,
			response.sendRedirect(viewPage);
		}
		// view Resolve end
	}

}

package co.micol.prj.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DataSource { // 싱글톤 클래스로 만들어야 함!
	private static SqlSessionFactory sqlSessionFactory;
	private DataSource() {}; // 외부에서 객체 생성하지 못하도록 프라이빗으로 생성자 만듦
	
	public static SqlSessionFactory getInstance() {
		String resource = "config/mybatis-config.xml"; // config파일 밑의 파일 이름이랑 같게 넣어준다!
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream); // 위에서 만든 싱글톤클래스를 가져와야함. 새로 생성하면 안됨. 주의하자!
		}catch(IOException e) {
			e.printStackTrace();
		}
		return sqlSessionFactory;
	}
}

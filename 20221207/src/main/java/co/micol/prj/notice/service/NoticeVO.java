package co.micol.prj.notice.service;

import java.sql.Date; // util로 넣으면 시분초까지 나옴. sql은 날짜까지만!

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeVO {
	private int noticeId;
	private String noticeWriter;
	private Date noticeDate;
	private String noticeTitle;
	private String noticeSubject;
	private int noticeHit; // 저장할때는 얘네만 씀.
	
	private int attechId; //join을 위한 확장
	private String noticeFile; //join을 위한 확장
	private String noticeFileDir; //join을 위한 확장
	
}

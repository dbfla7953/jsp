<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div align="center">
	<div><h1>회원정보 수정</h1></div>
	<div>
		<form id="frm" action="memberUpdate.do" method="post">
			<div>
				<table border="1">
							<tr>
							<th width="150">아이디</th>
							<td width="300">${member.memberId }</td> <!-- js에서는 id를, java에서는 name값을 가져가니까 꼭 같이 써준다! -->
						</tr>
						<tr>
							<th>이름</th>
							<td><input type="text" id="memberName" name="memberName" value="${member.memberName }"></td>
						</tr>
						<tr>
							<th>패스워드</th>
							<td><input type="password" id="memberPassword" name="memberPassword"></td>
						</tr>
						<tr>
							<th>나이</th>
							<td><input type="text" id="memberAge" name="memberAge" value="${member.memberAge }"></td>
						</tr>
						<tr>
							<th>주소</th>
							<td><input type="text" id="memberAddress" name="memberAddress" value="${member.memberAddress }"></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><input type="tel" id="memberTel" name="memberTel" value="${member.memberTel }"></td>
						</tr>
						<tr>
							<th>권 한</th>
							<td><input type="tel" id="memberAuthor" name="memberAuthor" value="${member.memberAuthor }"></td>
						</tr>
				</table>
			</div><br>
			<div>
				<input type="hidden" name="memberId" value="${member.memberId }">
				<input type="submit" value="수정">&nbsp;&nbsp;
				<input type="button" value="취소" onclick="location.href='memberList.do'">
			</div>
		</form>
	</div>
</div>
</body>
</html>
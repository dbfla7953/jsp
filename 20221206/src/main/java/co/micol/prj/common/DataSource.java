package co.micol.prj.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DataSource { // DAO를 관례적으로 DataSource라고 쓴다.
	private static SqlSessionFactory sqlSessionFactory;
	
	private DataSource() {}; // 외부에서 생성하지 못하도록 프라이빗으로 생성자 만듦.
	
	public static SqlSessionFactory getInstance() { // 인스턴스를 이용해서 가져갈 수 있도록 함.
		String resource = "config/mybatis-config.xml"; // 마이바티스 경로 맞춰서 수정
		InputStream inputStream;
		try {
			inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlSessionFactory; // 싱글톤 다 만들었다... 이게 싱글톤 기본
	}
}

package co.micol.prj.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command { 
	String exec(HttpServletRequest request, HttpServletResponse response); // request와 response객체를 전달받아서 String으로 돌려줌
}

package co.micol.prj.member.map;

import java.util.List;

import co.micol.prj.member.service.MemberVO;

public interface MemberMapper { // 매퍼는 xml로만듦
	List<MemberVO> memberSelectList(); // 전체조회
	MemberVO memberSelect(MemberVO vo); // 한 사람 조회 또는 로그인
	int memberInsert(MemberVO vo); // 입력
	int memberDelete(MemberVO vo); // 삭제
	int memberUpdate(MemberVO vo); // 수정
	
	boolean isIdCheck(String id); // 아이디 중복 체크 (이미 아이디 존재하면 펄스값 반환. 즉 변수만들때 디폴트를 펄스로 해놔야함)
}

// 멤버서비스를 그대로 복사.

package co.micol.prj;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.Command;
import co.micol.prj.member.command.MemberList;


@WebServlet("*.do")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HashMap<String, Command> map = new HashMap<String, Command>();
       

    public FrontController() {
        super();
    }


	public void init(ServletConfig config) throws ServletException {
		// 명령(Command)를 저장하는 영역
		 // 뭐가 들어왔는지 확인, 커맨드 실행. 인터페이스 스스로 초기화하지못하기때문에 구현체를 통해 초기화
		map.put("/main.do", new MainCommand()); // 처음페이지 보기
		map.put("/memberList.do", new MemberList()); // 멤버목록보기
	}


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Controller 본체
		request.setCharacterEncoding("utf-8"); // 한글깨짐 방지
		
		String uri = request.getRequestURI(); // uri값을 읽어옴
		String contextPath = request.getContextPath(); // contextPath값 읽어옴
		String page = uri.substring(contextPath.length()); // 실제요청명을 읽어옴 //요청분석
		
		Command command = map.get(page); // 커맨드는 인터페이스객체. command에서 어떤 요청을 수행할지 찾아옴
		String viewPage = command.exec(request, response); // 찾은 command를 실행
		
		//실행한 것을 어느 페이지에 보여주지? (돌려줄페이지를 찾음. 뷰리절브를 이렇게 만듦)
		if(!viewPage.endsWith(".do")) {
			// Ajax 처리하는 루틴
			viewPage = "WEB-INF/views/" + viewPage + ".jsp"; // web-inf는 서버에서만 접근가능. 
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(viewPage); // 리퀘스트 객체가 가장 마지막 페이지까지 전달됨.
			dispatcher.forward(request, response);
		}else { // .do로들어왔다면,
			response.sendRedirect(viewPage);
		}
	}

}

// 사실 이거 그대로 다른거에 써도 되지만, 직접 써서 익숙해지도록 한다

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- 이게 taglib 형식임 -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<jsp:include page="../menu/menu.jsp"/>
<body>
<div align="center">
	<div>회원목록</div>
	<div>
		<c:forEach items="${members }" var="m"> <!-- memberList.java가져옴(?) -->
			${m.memberId } : ${m.memberName } <!-- VO객체에 있는거랑 똑같이 써줘야함 -->
		</c:forEach>
		
	</div>
</div>
</body>
</html>